import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
// default
import registerServiceWorker from './registerServiceWorker';

// components
import Mainpage from './Main';
import Leaderboard from './Leaderboard';
import NotFound from './NotFound';
import Footer from './Footer';
import Header from './Header';

//styles
import './styles/index.scss';

const WithFooter = (props) => {
    return(
        <div className="mainContainer">
            <Header/>
            {props.children}
            <Footer/>
        </div>
    )
};

class Main extends Component {
    render() {
        return (
            <WithFooter>
                <Switch>
                    <Route exact path='/' component={Mainpage}/>
                    <Route exact path='/:game' component={Leaderboard}/>
                    <Route component={NotFound}/>
                </Switch>
            </WithFooter>
        )
    }
}

ReactDOM.render((
    <BrowserRouter>
        <Main/>
    </BrowserRouter>
), document.getElementById('root'));

registerServiceWorker();