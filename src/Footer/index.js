import React from 'react';

import {Socials,Support} from '../config';

import RipkordIcon from '../images/ripkord_icon.svg';
import './footer.scss';

const Footer = (props) => {
    return(
        <div className="footer">
            <div className="footer__divider">
                <img className="footer__icon" src={RipkordIcon} alt="Ripkord.tv icon"/>
            </div>
            <div className="footer__socialIcons">
                <a className="footer__socialIcon instagram link--icon" href={Socials.instagram} alt="Ripkord.tv Instagram channel"> </a>
                <a className="footer__socialIcon facebook link--icon" href={Socials.facebook} alt="Ripkord.tv Facebook group"> </a>
                <a className="footer__socialIcon twitter link--icon" href={Socials.twitter} alt="Ripkord.tv Twitter page"> </a>
                <a className="footer__socialIcon youtube link--icon" href={Socials.youtube} alt="Ripkord.tv YouTube channel"> </a>
            </div>
            <div className="footer__bottomText">
                <p>Contact us at <a className="footer__link link" href={`mailto:${Support.email}`}>{Support.email}</a></p>
                <p className="copyright">© 2018 Ripkord.tv</p>
            </div>
        </div>
    )
}

export default Footer;