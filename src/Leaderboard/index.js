import React,{Component} from 'react';
import {dataServices} from '../services/dataServices';
import {ErrorMsg, NotFoundMsg} from '../config';

import OnTheMoneyLogo from '../images/On_the_money.svg';
import RaiseTheBarLogo from '../images/Raise_the_bar.svg';

import Loading from '../Loading';
import Error from '../Error';

class Leaderboard extends Component{
    constructor(){
        super();
        this.state={
            game:'',
            loaded: false,
            error: false,
            data: [],
            notFound:false
        }
        this.dataServices = new dataServices();
    }

    componentDidMount(){
        const {game} = this.props.match.params;
        this.dataServices.getLeaderBoards(game).then(result => {
            console.log('Received and refined data',result);
            setTimeout(()=>{
                this.setState({
                    game:game,
                    loaded: true,
                    error: false,
                    data: result
                });
            },500);
        }).catch(error=>{
            console.log('Error in API',error);
            console.log('Error in API',error.message);
            //if API response with Not Found
            if (error.notFound) {
                this.setState({
                    notFound:true,
                    loaded: true,
                    error: false
                });
            } else {
                this.setState({
                    game:game,
                    loaded: true,
                    error: true
                });
            }
        })
    }

    render(){
        const {error,loaded,data,notFound} = this.state;
        return(
            <div>
                {!loaded && !error &&
                    <Loading/>
                }
                {loaded && error && !notFound &&
                    <Error mainText={ErrorMsg.title} description={ErrorMsg.description}/>
                }
                {loaded && notFound &&
                    <Error notFound={true} mainText={NotFoundMsg.title} description={NotFoundMsg.description}/>
                }
                {loaded && !error && !notFound &&
                    <div className="topBlock topBlock--onlyWaves topBlock--topSpace">
                        <div className="rk-table rk-table__header">
                            <div className="rk-table__cell rk-table__cell--mainCol">Player</div>
                            <div className="rk-table__cell no-phone">Game</div>
                            <div className="rk-table__cell">Amount</div>
                        </div>
                        {data.map(item =>
                            <div className="rk-table rk-table__row" key={item.username}>
                                <div className="rk-table__cell rk-table__cell--avatar">
                                    <div
                                        className="rk-avatar rk-avatar--table rk-avatar--inline rk-avatar--small"
                                        style={{backgroundImage:`url(${item.avatar})`}}
                                    ></div>
                                </div>
                                <div className="rk-table__cell rk-table__cell--mainCol block">
                                    {item.username}
                                </div>
                                <div className="rk-table__cell block">
                                    {item.gameType === '1' ? <img className="game-logo" src={OnTheMoneyLogo} alt="On The Money Logo"/> : <img className="game-logo" src={RaiseTheBarLogo} alt="Raise the Bar Logo"/>}
                                </div>
                                <div className="rk-table__cell rk-table__cell--importantData">
                                    {item.amount}
                                </div>
                            </div>
                        )}
                    </div>
                }
            </div>
        )
    }

}

export default Leaderboard;