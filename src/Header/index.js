import React from 'react';
import {NavLink} from 'react-router-dom';
import RipkordLogo from '../images/ripkord_icon.svg';
import './header.scss';

const Header = () => {
    return (
        <div className="rk-header-wrapper">
            <div className="rk-header rk-header__logo no-phone"><img src={RipkordLogo} alt="Ripkord.tv logotype"/></div>
            <div className="rk-header rk-header__navigation">
                <NavLink exact to="/" className="link link--navigation">Home</NavLink>
                <NavLink exact to="/leaderboard" className="link link--navigation" activeClassName="active">Leaderboard</NavLink>
            </div>
        </div>
    )
}

export default Header;
