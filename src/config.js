export const AppStore = {
    apple: 'http://bit.ly/ripkord',
    android: '#'
};

export const Socials = {
    facebook: 'http://facebook.com',
    instagram: 'http://facebook.com',
    twitter: 'http://twitter.com',
    youtube: 'http://youtube.com'
};

export const Support = {
    email:'support@ripkord.tv'
};

// http://localhost:3004/ — local json-server
export const API = {
    apiServer: 'http://dev.strimmerz.com:8080/'
};

export const ErrorMsg = {
    title: 'Something wrong',
    description:'Please, try again later'
};

export const NotFoundMsg = {
    title: 'Leaderboard Not Found',
    description:'Please, check URL'
};