import React from 'react';
import ErrorIcon from '../images/Error.svg';
import LeaderBoard from '../images/Leader_board.svg';
import './error.scss';

const Error = (props) => {
    const icon = props.notFound ? LeaderBoard : ErrorIcon;
    const mainText = props.mainText || 'Default Header';
    const description = props.description || 'Default Description';
    return(
        <div className="rk-error topBlock topBlock--onlyWaves topBlock--topSpace">
            <div className="rk-error__content">
                <img src={icon} alt="Ripkord.tv Something wrong" className="rk-error__icon"/>
                <div className="rk-error__message">{mainText}</div>
                <div className="rk-error__message rk-error__message--description">{description}</div>
            </div>
        </div>
    )
}

export default Error;