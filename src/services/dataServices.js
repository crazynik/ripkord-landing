import axios from 'axios';
import {API} from '../config';

export class dataServices {
    getAPI(server,endpoint){
        return axios.get(server + endpoint);
    }

    resortLeaderBoard(data){
        if(data){
            return (
                data.map(item => ({
                    username: item.username,
                    avatar: item.avatar,
                    amount: item.amount,
                    gameType: item.gameType
                }))
            )
        } else {
            return [];
        }
    }

    getLeaderBoards(endpoint){
        return new Promise((respond,reject)=>{
            this.getAPI(API.apiServer, endpoint).then(result=>{
                //might need to refine
                respond(this.resortLeaderBoard(result.data.leaderboard[0]));
            }).catch(error =>{
                const refineError = {
                    message: error.stack,
                    notFound: error.response && error.response.status === 404
                };
                reject(refineError);
            })
        });
    }
}