import React from 'react';
import './loading.scss';

const Loading = () => {
    return(
        <div className="rk-loading topBlock topBlock--onlyWaves topBlock--topSpace">
            <div className="rk-loading__content">
                <div className="rk-loading__spinner sk-cube-grid">
                    <div className="sk-cube sk-cube1"></div>
                    <div className="sk-cube sk-cube2"></div>
                    <div className="sk-cube sk-cube3"></div>
                    <div className="sk-cube sk-cube4"></div>
                    <div className="sk-cube sk-cube5"></div>
                    <div className="sk-cube sk-cube6"></div>
                    <div className="sk-cube sk-cube7"></div>
                    <div className="sk-cube sk-cube8"></div>
                    <div className="sk-cube sk-cube9"></div>
                </div>
                <div className="rk-loading__message">Loading</div>
            </div>
        </div>
    )
}

export default Loading;