import React from 'react';
import TopBlock from './TopBlock';
import Features from './Features';

const Main = () => {
    return(
        <div>
            <TopBlock/>
            <Features/>
        </div>
    )
}

export default Main;