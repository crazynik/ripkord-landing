import React from 'react';

import {AppStore} from '../config';
import RipkordLogo from '../images/ripkord_logo.svg';

import './topBlock.scss';

const checkDevice = () => {
    const device = navigator.userAgent;
    if (device.match(/Android/i)){
        return 'android';
    } else if (device.match(/iPhone/i) || device.match(/iPhone/i) || device.match(/iPad/i) || device.match(/iPod/i)){
        return 'apple';
    } else {
        //by default
        return 'other';
    }
}



const TopBlock = () => {
    const device = checkDevice();

    const getButtons = () => {
        if (device === 'other') {
            return(
                <div className="twoButtons">
                    <a className={`topBlock__button link--icon apple`} href={AppStore.apple}> </a>
                    <a className={`topBlock__button link--icon android`} href={AppStore.android}> </a>
                </div>
            )
        } else {
            return <a className={`topBlock__button link--icon ${device}`} href={AppStore[device]}> </a>
        }
    };

    return(
        <div className="topBlock">
            <div className={`topBlock__content ${device}`}>
                <img className="topBlock__logo" src={RipkordLogo} alt="Ripkord.tv Logo"/>
                <div className="topBlock__slogan">
                    Live, Interactive<br/>
                    Mobile TV
                </div>
                {getButtons()}
            </div>
            <div className={`topBlock__phone ${device}`}></div>
            <div className="topBlock__divider"></div>
        </div>
    )
}

export default TopBlock;