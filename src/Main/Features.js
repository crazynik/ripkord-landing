import React from 'react';

import LiveHost from '../images/Users_icon.svg';
import RealPrize from '../images/Prize_icon.svg';
import FastGame from '../images/Fast_icon.svg';

import './features.scss';

const Features = (props) => {
    return(
        <div className="featureBlock">
            <div className="feature">
                <img className="feature__icon" src={LiveHost} alt="Live Host Icon"/>
                <p className="feature__title">Live Host</p>
            </div>
            <div className="feature">
                <img className="feature__icon" src={RealPrize} alt="Live Host Icon"/>
                <p className="feature__title">Win Real Money
                </p>
            </div>
            <div className="feature">
                <img className="feature__icon" src={FastGame} alt="Live Host Icon"/>
                <p className="feature__title">Fast Games</p>
            </div>
        </div>
    )
}

export default Features;